var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/clientes', function(req, res){
  //res.send('HOLA MUNDO');
  res.sendFile(path.join(__dirname, 'clientes.html'));
});

app.post('/clientes/:idCliente', function(req, res){
  res.send('CLIENTE INSERTADO: '+req.params.idCliente);
});

app.put('/clientes/:idCliente', function(req, res){
  res.send('CLIENTE MODIFICADO 2: '+req.params.idCliente);
});

app.delete('/clientes/:idCliente', function(req, res){
  res.send('CLIENTE ELIMINADO: '+req.params.idCliente);
});
